# Jarvis （人工智能助理）
### 灵感来源
电影《钢铁侠》中的智能软件Jarvis。
### 简述
> 一款以全新的人机交互方式提供服务的人工智能软件，能够以语音和GUI等方式作为输入来方便地控制操作系统和IoT设备。同时有一个二次元3D形象作为前端。提供插件编程接口，可以开发更多的功能组件。
### 特性
1. 跨平台，支持Windows，Linux，Mac，Android等等.源代码一键无脑编译。
2. 可拓展，支持插件，支持热更新。用户也可以自己编写插件。基于QtPluginLoader。安卓平台不支持。
3. 有二次元的人形界面，符合当下审美潮流，吸引用户。可自定义外观，动作。
4. 支持轮询对话，基于强大的科大讯飞sdk，支持语音唤醒，支持自动监听和自动停止。
5. ...
### 开源许可
本源代码采用QT5社区版本开发，一切解释权归原作者所有。
基于本项目或者修改自本项目的软硬件都需要开源。
技术与创意是大家的，禁止将相关技术盈利化。
### 联系方式
QQ：2836365231 QQ群：221359737
### 演示视频
[用计算机控制智能家居](https://www.bilibili.com/video/av58799269/)
[用智能手环控制智能家居](https://www.bilibili.com/video/av61263445/)
[本软件使用演示](https://www.bilibili.com/video/av61264036/)
### 屏幕截图
![Windows下原尺寸的显示效果](https://images.gitee.com/uploads/images/2019/0808/112118_98b90fee_632560.png "c.png")
![Windows下可变尺寸的显示效果](https://images.gitee.com/uploads/images/2019/0808/105712_7ce0ceca_632560.png "a.png")
![Linux环境下的显示效果](https://images.gitee.com/uploads/images/2019/0808/105824_6c375c33_632560.png "b.png")
![Android平台下的显示效果](https://images.gitee.com/uploads/images/2019/0808/105853_579e61fa_632560.png "d.png")
### 编译开发运行
到QT官网下载最新版本的QT creator打开项目即可编译运行。
### 项目结构
- Bullet （开源的物理引擎，静态库）
- Saba （mmd文件格式读取，静态库）
- JarvisInterface （Jarvis插件接口，静态库，插件编写都要基于此）
- JarvisAgent （三维人形前端，动态库，插件）
- JarvisHome （基于TCP协议的智能家居通讯样例，动态库，插件）
- JarvisSpeech （基于科大讯飞的语音模块，有识别，播放，语义功能，动态库，插件）
- Jarvis （主程序，提供插件运行环境，可执行文件）
- TestOffScreenRender （测试QT平台的Opengl渲染，可执行文件，可以删除本子项目）
- Testinterface （测试Jarvis插件，不会编写插件的可以参考这个模板，可删除，动态库）
### 最后
 :wink: 本人是在校大学生，代码不易，能否给个Star？？？顺便关注下本人B站？
代码中提供的apikey调用次数有限，请更换成自己的。
