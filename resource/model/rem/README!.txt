﻿REM-maple式初音ミクV3 Ver1.00

Hi!REMmapleです!

モデルComplete! 本当にうれしいです!
日本人ではない、READMEにはよく書いているので、許しください m(_ _)m

●英語のルールをご参照ください


JP【このモデルの利用規約】
EN【Rules】
CN【此模型的使用条款】
IT【Condizioni e regolamenti d'uso】



JP【改造, 再配布    OKです】（再配布する際はREADMEファイルに改造元（元のモデル名や作者名）が分かるように明記して下さい。）
EN【Editing and Redistribution is OK】But you must credit the author（REMmaple） and Note me on Twitter（@REMmaple） or on DeviantArt（REMmaple）
CN【改造和配布是允许的】不过在配布时必须要署名（原作者REMmaple），并且联系本人Twitter（@REMmaple）,DeviantArt(REMmaple) 或者 百度贴吧（萌萌的张尼玛）
IT【Editing e riditribuzione è OK】Però devi nominare l'autore originario（REMmaple） e conttatarmi su Twitter （@REMmaple） oppure su DeviantArt(REMmaple)


JP【商業化      禁止です】（このデータを使用したことにより何らかのトラブルが発生した場合でも、当方は一切の責任を負いません。）
EN【Commercial usage is forbidden】If you broke this rule, the responsibility is all yours.
CN【严禁商业化】嘿。。嘿嘿。。嘿嘿嘿呵呵哈哈哈，谁这么没节操商业化的话后果自负
IT【Vietato uso commerciale】Le responsabilità sono tue se decidi di infrangere questa regola


JP【公序良俗に反する使用（暴力や18禁や）   禁止です】
EN【Using this model in VIOLENCE or/and R18 is forbidden】
CN【亲们请不要使用这个模型做无节操(当然只是R18)和暴力的视频】
IT【Vietato l'uso di questo modulo nei temi di VIOLENZA o di SESSUALITA'（R18）】


JP【初音ミクはクリプトン.フューチャー.メディア株式会社の創作物であり、利用の際はピアプロ.キャラクター.ライセン　スに従う必要があります。】
http://piapro.jp/license/character_guideline

EN【Hatsune Miku is copyrighted by Crypton Future Media inc. please respect their rules on this character and read 】
http://piapro.jp/license/character_guideline

CN【相信你们翻墙不过去就算了 - =【喂喂泥垢！【初音未来是由Crypton Future Media inc.创造所以请遵守http://piapro.jp/license/character_guideline的规则】

IT【Hatsune Miku è stata creata da Crypton Future Media, quindi vi prego di leggere le seguenti regole http://piapro.jp/license/character_guideline】







【When you credit you edited model I prefer see this↓】


3D Model by REM-maple
Illustrations and design by iXima
Hatsune Miku by Crypton Future Media inc.






