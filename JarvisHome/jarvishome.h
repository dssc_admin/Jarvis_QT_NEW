#ifndef JARVISHOME_H
#define JARVISHOME_H

#include "jarvisinterface.h"
#include "ui_form.h"
#include <QTcpSocket>
#include <QJsonArray>

class  JarvisHome : public JarvisInterface
{
    Q_OBJECT
#ifdef Q_OS_ANDROID

#else
    Q_PLUGIN_METADATA(IID "com.finch.JarvisHome" FILE "test.json")
    Q_INTERFACES(JarvisInterface)
#endif

public:
    JarvisHome();
    void Init(QString resPath);
    QWidget * GetWidget(QWidget * parent);
    void PollMessage(Type from,QJsonObject data);
    ~JarvisHome();
public:
    void ChangeState(bool state);
public slots:
    void on_pushButton_clicked();
private:
    Ui::Form *ui;
    QTcpSocket *client;
    bool state = false;
};

#endif // JARVISHOME_H
