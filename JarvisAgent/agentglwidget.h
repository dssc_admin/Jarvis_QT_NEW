#ifndef AGENTGLWIDGET_H
#define AGENTGLWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>

#include "mmdscene.h"
#include "mmdshader.h"
#include "mmdmaterial.h"
#include "mmdmodel.h"

class AgentGLWidget : public QOpenGLWidget ,protected QOpenGLFunctions{
public:
    explicit AgentGLWidget(QString resDir,QWidget * parent = nullptr);
    virtual ~AgentGLWidget();
public:
    MMDScene *mmdScene;
    Model * model;
    QMap<QString,saba::VMDFile> motions;
    float sum_idle_time = 0.0f;
    QString cur_anim;
public:
    void ChangeMonth(bool state);
    void InitMotions();
    void AutoPLayIdle();
    void AddMotions(QString Key,QString filePath);
    void SetMotions(QString Key,bool Loop = false,bool Clear = true);
private:
    const static unsigned int FPS = 60;
    QString resourceDir;
protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
};

#endif // AGENTGLWIDGET_H
