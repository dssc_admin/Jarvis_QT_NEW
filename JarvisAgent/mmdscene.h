#ifndef MMDSCENE_H
#define MMDSCENE_H

#include <QOpenGLTexture>
#include <QMap>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "mmdshader.h"

class MMDScene{
public:
    MMDShader *shader;
    QMap<QString,QOpenGLTexture *> textures;

    glm::mat4	m_viewMat;
    glm::mat4	m_projMat;
    glm::vec3	m_lightColor = glm::vec3(1, 1, 1);
    glm::vec3	m_lightDir = glm::vec3(-0.5f, -1.0f, -0.5f);
public:
    QOpenGLTexture * GetTexture(QString filepath);
    MMDScene(QString vertexFile,QString fragFile);
    ~MMDScene();
};

#endif // MMDSCENE_H
